from django.shortcuts import render
from django.http import HttpResponse
import csv
import json


def index(request):

    if request.method == 'POST':
        word = request.POST.get('select_word')
        cnt = 0
        response_dict = {}
        data = {}
        data['nodes'] = []
        data['links'] = []
        with open('static/files/graph.csv', 'rt', encoding='utf8') as f:
            reader = csv.reader(f, delimiter=',')
            for row in reader:
                if word == row[1]:
                    if cnt == 0:
                        data['nodes'].append({
                            'name': word,
                            'group': 0,
                            'number': cnt
                        })
                    cnt = cnt+1
                    data['nodes'].append({
                        'name': row[2],
                        'group': 1,
                        'number': cnt
                    })
                    data['links'].append({
                        'source': 0,
                        'target': cnt,
                        'value': row[3]
                    })
        if not(bool(data['links']) and bool(data['nodes'])):
            response_dict['error'] = True
        response_dict['data'] = data
        return HttpResponse(json.dumps(response_dict), content_type="application/json")
    else:
        return render(request, 'index.html', {})
